package com.example.world.controller;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class worldcontroller {
	
	@GetMapping("/hi")
	public String gethello()
	{
		return "Hello world";
	}
	
	@PostMapping("/hai")
	public String secondMethod(@RequestParam String name)
	{
		return "Hello:"+name;
		
	}
	
	//The variable and parameter different
	@GetMapping("/Id/name")
	public String studentDetails(@RequestParam(name ="id") String studentid,@RequestParam String name )
	{
		return "ID: "+studentid+ "Name: "+name;
		
	}
	
	@GetMapping("/progress")
	public String studetProgress(@RequestParam(required=false) String studentresult)
	{
		return "Result: "+studentresult;
		
	}
	
	@GetMapping("/dept")
	public String studentdept(@RequestParam(defaultValue="mech") String studentdepartment)
	{
		return studentdepartment;
		
	}
	
	@GetMapping("/std")
	public String studentstd(@PathVariable (required=false) String Studentard) {
		return "StudentSTD:"+Studentard;
		
	}
	
	@GetMapping("/regsiter")
	public String studentreg(@PathVariable String regno)
	{
		return "Studentregno:"+regno;
		
	}
	
	@GetMapping("/Studentfee")
	public String studentfeesdetails(@PathVariable(value ="addfee") double feedetail) {
		return "Student fees details:"+String.valueOf(feedetail);
		
	}

}